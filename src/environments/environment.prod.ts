export const environment = {
  production: true,
  apiUrl: "https://livecareapi.azurewebsites.net/api",
  baseImgUrl: "https://livecareapi.azurewebsites.net/",
  defaultPageSize: 10,
  characterImg: "./assets/images/character.png",
};
