import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DatePipe } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ComponentsModule } from "./components/components.module";
import { LayoutModule } from "./shared/layout/layout.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxSpinnerModule } from "ngx-spinner";
import { LoadingBarRouterModule } from "@ngx-loading-bar/router";
import { AuthModule } from "./auth/auth.module";
import { ToastrModule } from "ngx-toastr";
import { AuthGuard } from "./shared/guards/auth.guard";
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
} from "angularx-social-login";
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angularx-social-login";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { JwtInterceptor } from "./shared/helpers/jwt.interceptor";
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";
import { PatientCallingComponent } from "./patient-calling/patient-calling.component";
import { PatientCallingModule } from "./patient-calling/patient-calling.module";

@NgModule({
  declarations: [AppComponent, PatientCallingComponent],
  imports: [
    BrowserModule,
    ComponentsModule,
    LayoutModule,

    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    LoadingBarRouterModule,
    AuthModule,
    SocialLoginModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: "toast-bottom-right",
      preventDuplicates: true,
    }),
    HttpClientModule,
    MatPasswordStrengthModule,
    AppRoutingModule,
  ],

  providers: [
    DatePipe,
    AuthGuard,
    {
      provide: "SocialAuthServiceConfig",
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              "63856934160-d8jeeup0n0rmja6j6ohmvgpmo69i6qh3.apps.googleusercontent.com"
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider("363141278414880"),
          },
        ],
      } as SocialAuthServiceConfig,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
