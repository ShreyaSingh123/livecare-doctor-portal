import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentsReportsComponent } from './attachments-reports.component';

describe('AttachmentsReportsComponent', () => {
  let component: AttachmentsReportsComponent;
  let fixture: ComponentFixture<AttachmentsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
