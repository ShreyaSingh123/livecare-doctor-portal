import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../shared/guards/auth.guard";
import { MedicalRecordsComponent } from "./medical-records.component";
import { PastMedicalHistoryComponent } from "./past-medical-history/past-medical-history.component";
import { SurgicalHistoryComponent } from "./surgical-history/surgical-history.component";
import { FamilyHistoryComponent } from "./family-history/family-history.component";
import { AllergyComponent } from "./allergy/allergy.component";
import { AttachmentsReportsComponent } from "./attachments-reports/attachments-reports.component";

const routes: Routes = [
  {
    path: "",
    component: MedicalRecordsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "past-medical-history",
    component: PastMedicalHistoryComponent,
  },
  {
    path: "surgical-history",
    component: SurgicalHistoryComponent,
  },
  {
    path: "family-history",
    component: FamilyHistoryComponent,
  },
  {
    path: "allergy",
    component: AllergyComponent,
  },
  {
    path: "attachments-reports",
    component: AttachmentsReportsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalRecordsRoutingModule {}
