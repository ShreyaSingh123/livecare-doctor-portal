export * from "./allergy/allergy.component";
export * from "./attachments-reports/attachments-reports.component";
export * from "./family-history/family-history.component";
export * from "./past-medical-history/past-medical-history.component";
export * from "./surgical-history/surgical-history.component";
export * from "./medical-records.component";
