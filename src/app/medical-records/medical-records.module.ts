import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MedicalRecordsRoutingModule } from "./medical-records-routing.module";
import {
  AllergyComponent,
  AttachmentsReportsComponent,
  FamilyHistoryComponent,
  MedicalRecordsComponent,
  PastMedicalHistoryComponent,
  SurgicalHistoryComponent,
} from ".";

@NgModule({
  declarations: [
    MedicalRecordsComponent,
    PastMedicalHistoryComponent,
    SurgicalHistoryComponent,
    FamilyHistoryComponent,
    AllergyComponent,
    AttachmentsReportsComponent,
  ],
  imports: [CommonModule, MedicalRecordsRoutingModule],
})
export class MedicalRecordsModule {}
