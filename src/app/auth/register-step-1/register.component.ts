import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";
declare let $: any;
import { DatePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import * as moment from "moment";
import {
  ConfigurationOptions,
  ContentOptionsEnum,
  NumberResult,
  OutputOptionsEnum,
  SortOrderEnum,
} from "intl-input-phone";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  maxDate = new Date();
  submitted = false;
  dialCodeValue: any;
  data: any;
  genderId: any;
  otp: any;
  configOption: ConfigurationOptions;
  OutputValue2: NumberResult = new NumberResult();
  requiredDailCode: string = "";
  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {
    this.configOption = new ConfigurationOptions();
    this.configOption.SelectorClass = "OptionType3";
    this.configOption.SortBy = SortOrderEnum.CountryName;
    this.configOption.OptionTextTypes = [];
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.Flag);
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.CountryName);
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.CountryPhoneCode);
    this.configOption.OutputFormat = OutputOptionsEnum.Number;
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      dateOfBirth: ["", Validators.required],
      genderId: ["", Validators.required],
      phoneNumber: ["", Validators.required],
    });
    if (localStorage.getItem("resigterData") == null) {
    } else {
      var resigterData = JSON.parse(localStorage.getItem("resigterData"));
      this.registerForm.controls["firstName"].setValue(resigterData.firstName);
      this.registerForm.controls["lastName"].setValue(resigterData.lastName);
      this.registerForm.controls["dateOfBirth"].setValue(
        moment(resigterData.dateOfBirth, "DD/MM/YYYY").format("MM/DD/YYYY")
      );
      this.registerForm.controls["genderId"].setValue(
        parseInt(resigterData.genderId)
      );
      this.requiredDailCode =
        resigterData.phoneNumber.CountryModel.CountryPhoneCode +
        " " +
        resigterData.phoneNumber.Number;
      this.registerForm.controls["phoneNumber"].setValue(
        resigterData.phoneNumber
      );
    }
  }
  get registerFormError() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    var data = {
      firstName: this.registerForm.controls["firstName"].value,
      lastName: this.registerForm.controls["lastName"].value,
      genderId: parseInt(this.registerForm.controls["genderId"].value),
      dateOfBirth: this.datePipe.transform(
        this.registerForm.controls["dateOfBirth"].value,
        "dd-MM-yyyy"
      ),
      phoneNumber: this.registerForm.controls["phoneNumber"].value,
      deviceType: new FormControl(ConstantValues.consDeviceType).value,
      deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
    };
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.spinner.hide();
      return;
    }
    localStorage.setItem("resigterData", JSON.stringify(data));
    this.router.navigateByUrl("/register/step-2");
    this.spinner.hide();
  }

  onEnterNumber(outputResult) {
    this.OutputValue2 = outputResult;
  }
  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }
}
