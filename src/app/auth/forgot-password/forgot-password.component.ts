import { Component, OnInit } from "@angular/core";
import { FormGroup, FormArray, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/auth/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"],
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted = false;
  router: any;
  toasterervice: any;
  otp: any;
  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) {
        control.markAsTouched();
        this.markFormTouched(control);
      } else {
        control.markAsTouched();
      }
    });
  }
  constructor(
    fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private route: Router
  ) {
    this.forgotPasswordForm = fb.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            /^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/
          ),
        ]),
      ],
    });
  }

  ngOnInit() {}

  onForgotPassword() {
    this.spinner.show();
    this.submitted = true;
    if (this.forgotPasswordForm.invalid) {
      this.spinner.hide();
      return;
    }
    this.markFormTouched(this.forgotPasswordForm);
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.authService
      .forgotPassword(this.forgotPasswordForm.value)
      .subscribe((data) => {
        if (data.status) {
          this.spinner.hide();
          this.otp = data.data.otpCode;
          localStorage.setItem("otp", this.otp);

          this.toastr.success(data.message);

          this.route.navigateByUrl("/reset");
          //  alert('your otp is ' + data.data.otpCode)
        } else {
          this.spinner.hide();
          this.toastr.error(data.message);
        }
      });
  }
}
