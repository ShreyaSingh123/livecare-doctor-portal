// Angular Inbuilt Package
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Angular Routing and Forms Module
import { AuthRoutingModule } from "./auth-routing.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

// Modules
import { MaterialModule } from "../shared/main-material/material/material.module";
import SharedModule from "../shared/shared.module";
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";

// Third Party Packages
import { Ng2TelInputModule } from "ng2-tel-input";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { NgxMultiSelectModule } from "@ngx-tiny/multi-select";
import { IntlInputPhoneModule } from "intl-input-phone";
import { NgOtpInputModule } from "ng-otp-input";
import { SelectDropDownModule } from "ngx-select-dropdown";

// Auth Components
import {
  LoginComponent,
  RegisterComponent,
  ForgotPasswordComponent,
  ResetPasswordComponent,
  VerifyEmailComponent,
  Register2Component,
  BasicInfoComponent,
  ChangePasswordComponent,
} from ".";

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    VerifyEmailComponent,
    Register2Component,
    BasicInfoComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    IntlInputPhoneModule,
    NgxMultiSelectModule,
    NgOtpInputModule,
    MatPasswordStrengthModule.forRoot(),
    SelectDropDownModule,
    BsDatepickerModule.forRoot(),
    Ng2TelInputModule,
    MaterialModule,
  ],
  exports: [],
  providers: [],
})
export class AuthModule {}
