import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

import { AuthService } from "src/app/auth/auth.service";
import { ConstantValues } from "src/app/shared/enums/constant-values.enum";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import {
  SocialAuthService,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angularx-social-login";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  socialloginForm: FormGroup;
  googleLoginDetails: any;
  facebookLoginDetials: any;
  otp: any;
  email: any;
  show: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private authService: SocialAuthService
  ) {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
      window.history.pushState(null, "", window.location.href);
    };

    this.show = false;
    localStorage.clear();
  }

  ngOnInit() {
    if (this.auth.userValue == null) {
      this.router.navigate(["/login"]);
      localStorage.clear();
    } else {
      this.router.navigate(["/dashboard"]);
    }
    this.loginForm = this.formBuilder.group({
      email: [
        "jennifer@mailinator.com",
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      password: [
        "Tester@123",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
      deviceType: new FormControl(ConstantValues.consDeviceType),
      deviceToken: new FormControl(ConstantValues.consDeviceToken),
    });
  }
  // convenience getter for easy access to form fields

  get loginFormError() {
    return this.loginForm.controls;
  }
  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.spinner.hide();
      return;
    }
    // display form values on success
    this.auth.login(this.loginForm.value).subscribe((res) => {
      this.otp = res.data.otp;
      localStorage.setItem("otp", this.otp);
      this.email = res.data.email;
      localStorage.setItem("email", this.email);
      if (res.status) {
        localStorage.setItem("token", res.data.accessToken);
        switch (res.data.lastScreenId) {
          case 1: {
            this.router.navigate(["verify"]);
            this.spinner.hide();
            break;
          }
          case 2: {
            this.router.navigate(["basic-info"]);
            this.spinner.hide();
            break;
          }
          case 3: {
            this.router.navigate(["/dashboard"]);
            localStorage.removeItem("token");
            localStorage.removeItem("email");
            localStorage.removeItem("otp");
            break;
          }
        }
      } else {
        if (res.status == false) {
          if (res.data.lastScreenId) {
            localStorage.setItem("token", res.data.accessToken);
            this.router.navigateByUrl("verify");
            this.spinner.hide();
          } else {
            localStorage.clear();
            this.toastr.error(res.message);
            this.spinner.hide();
            return;
          }
        }
      }
    });
  }

  password() {
    this.show = !this.show;
  }

  signInWithGoogle(value: any) {
    this.authService
      .signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((googleLoginDetails) => {
        this.googleLoginDetails = googleLoginDetails;
        this.socialloginForm = this.formBuilder.group({
          firstName: this.googleLoginDetails.firstName,
          lastName: this.googleLoginDetails.lastName,
          LoginType: this.googleLoginDetails.provider,
          email: this.googleLoginDetails.email,
          socialId: this.googleLoginDetails.id,
          deviceType: new FormControl(ConstantValues.consDeviceType).value,
          deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
        });
        this.auth.googleLogin(this.socialloginForm.value).subscribe((res) => {
          if (res.status) {
            this.toastr.success(res.message);
            this.router.navigateByUrl("/dashboard");
            this.spinner.hide();
          } else {
            this.spinner.hide();
            this.toastr.warning(res.message);
          }
        });
      });
  }

  signInWithFacebook() {
    this.authService
      .signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((facebookLoginDetials) => {
        this.facebookLoginDetials = facebookLoginDetials;
        this.socialloginForm = this.formBuilder.group({
          firstName: this.facebookLoginDetials.firstName,
          lastName: this.facebookLoginDetials.lastName,
          LoginType: this.facebookLoginDetials.provider,
          email: this.facebookLoginDetials.email,
          socialId: this.facebookLoginDetials.id,
          deviceType: new FormControl(ConstantValues.consDeviceType).value,
          deviceToken: new FormControl(ConstantValues.consDeviceToken).value,
        });
        this.auth.googleLogin(this.socialloginForm.value).subscribe((res) => {
          if (res.status) {
            this.router.navigateByUrl("/dashboard");
            this.spinner.hide();
          } else {
            this.spinner.hide();
            this.toastr.warning(res.message);
          }
        });
      });
  }
}
