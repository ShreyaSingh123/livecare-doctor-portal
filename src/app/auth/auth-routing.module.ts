import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//Components
import {
  LoginComponent,
  RegisterComponent,
  Register2Component,
  ForgotPasswordComponent,
  ResetPasswordComponent,
  VerifyEmailComponent,
  BasicInfoComponent,
  ChangePasswordComponent,
} from ".";

//Shared
import { AuthGuard } from "../shared/guards/auth.guard";

//Components

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register/step-1",
    component: RegisterComponent,
  },
  {
    path: "register/step-2",
    component: Register2Component,
  },
  {
    path: "forgot",
    component: ForgotPasswordComponent,
  },

  {
    path: "reset",
    component: ResetPasswordComponent,
  },
  {
    path: "change-password",
    component: ChangePasswordComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "verify",
    component: VerifyEmailComponent,
  },
  {
    path: "basic-info",
    component: BasicInfoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
