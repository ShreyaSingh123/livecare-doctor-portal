import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/auth/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.css"],
})
export class ResetPasswordComponent implements OnInit {
  otp: string;
  resetForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private route: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.otp = localStorage.getItem("otp");
    this.resetForm = this.formBuilder.group({
      otp: ["", Validators.required],
      newPassword: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
      email: ["", [Validators.required, Validators.email]],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetForm.controls;
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;

    var data = {
      otp: this.resetForm.controls["otp"].value,
      newPassword: this.resetForm.controls["newPassword"].value,
      email: this.resetForm.controls["email"].value,
    };

    // stop here if form is invalid
    if (this.resetForm.invalid) {
      this.spinner.hide();
      return;
    }

    this.auth.onResetPassword(data).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("/login");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }
}
