import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { LoginComponent } from "src/app/auth/login/login.component";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(
    private http: HttpClient,
    private router: Router,
    private toaster: ToastrService
  ) {
    this.currentUserSubject = new BehaviorSubject<LoginComponent>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get userValue(): any {
    return this.currentUserSubject.value;
  }
  getToken() {
    return localStorage.getItem("currentUser");
  }
  isLoggedIn() {
    return this.getToken() !== null;
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }
  // Doctor Login
  login(data: any) {
    return this.http
      .post<any>(environment.apiUrl + "/" + ApiEndPoint.login, data)
      .pipe(
        map((user) => {
          if (user.status) {
            if (user.data.lastScreenId == 3) {
              localStorage.setItem("currentUser", JSON.stringify(user));
              this.currentUserSubject.next(user);
            } else {
              this.router.navigate(["basic-info"]);
              this.toaster.warning("Please Complete Basic Profile Information");
            }
          } else {
            this.toaster.error(user.message);
          }
          return user;
        })
      );
  }

  // Doctor Register
  register(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.register,
      data
    );
  }

  // Doctor Forgot-Password
  forgotPassword(email: string) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.forgotPassword,
      email
    );
  }

  // Reset Password
  onResetPassword(resetPassword: any) {
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.resetPassword,
        resetPassword
      )
      .pipe(
        map((user) => {
          return user;
        })
      );
  }

  googleLogin(googleLoginDetials: any) {
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.googleLogin,
        googleLoginDetials
      )
      .pipe(
        map((user: any) => {
          if (user.status) {
            user.data.accessToken = user.data.accessToken;
            localStorage.setItem("currentUser", JSON.stringify(user));
          } else {
            return user;
          }
          return user;
        })
      );
  }

  facebookLogin(facebookLoginDetials: any) {
    return this.http
      .post<any>(
        environment.apiUrl + "/" + ApiEndPoint.facebookLogin,
        facebookLoginDetials
      )
      .pipe(
        map((user: any) => {
          if (user.status) {
            user.data.accessToken = user.data.accessToken;
            localStorage.setItem("currentUser", JSON.stringify(user));
          } else {
            return user;
          }
          return user;
        })
      );
  }

  // On Doctor Logout
  logout() {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.logout,
      null
    );
  }

  //On change password
  onChangePassword(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.changePassword,
      data
    );
  }

  //On verify email
  onVerifyEmail(data: any) {
    var headers_object = new HttpHeaders().set(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.verifyEmail,
      data,
      { headers: headers_object }
    );
  }
  //On resend email
  resendEmailCode(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.resendEmailCode,
      data
    );
  }
  // update Profile Pic
  updateProfilePic(imgFile: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateProfilePic,
      imgFile
    );
  }

  getDoctorDashboardInfo(): Observable<any> {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDoctorDashboardInfo
    );
  }

  //Create blog
  CreateBlog(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.CreateBlog,
      data
    );
  }

  //getBlogList
  getBlogList(pageNumber: any, pageSize: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getBlogs +
          "?pageNumber=" +
          pageNumber +
          "&pageSize=" +
          pageSize
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  // getCountries
  getCountries() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getCountries)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  // getNationalities
  getNationalities() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getNationalities)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  // getSpecility
  getSpecility() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getSpecility)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  //getStates
  getStates(countryId: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getStates +
          "?CountryId=" +
          countryId
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  addDoctorSpecialityInfo(data: any) {
    var headers_object = new HttpHeaders().set(
      "Authorization",
      "Bearer " + localStorage.getItem("token")
    );

    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.addDoctorSpecialityInfo,
      data,
      { headers: headers_object }
    );
  }
}
