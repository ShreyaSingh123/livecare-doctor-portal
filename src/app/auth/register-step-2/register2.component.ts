import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-register2",
  templateUrl: "./register2.component.html",
  styleUrls: ["./register2.component.css"],
})
export class Register2Component implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  showDetails: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private toastr: ToastrService,
    private changeDetector: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    var previousData = localStorage.getItem("resigterData");
    if (previousData == null) {
      this.router.navigateByUrl("login");
    }
    this.registerForm = this.formBuilder.group({
      email: [
        "",
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ],
      ],
      acceptTerms: ["", Validators.requiredTrue],
    });
  }

  get registerFormError() {
    return this.registerForm.controls;
  }
  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.spinner.hide();
      return;
    }
    var previousData = JSON.parse(localStorage.getItem("resigterData"));
    let resgigterData = {
      firstName: previousData.firstName,
      lastName: previousData.lastName,
      email: this.registerForm.controls["email"].value,
      password: this.registerForm.controls["password"].value,
      genderId: previousData.genderId,
      dateOfBirth: previousData.dateOfBirth,
      phoneNumber: previousData.phoneNumber.Number,
      deviceType: previousData.deviceType,
      deviceToken: previousData.deviceToken,
      dialCode: previousData.phoneNumber.CountryModel.CountryPhoneCode.replace(
        /[^A-Z0-9]/gi,
        ""
      ),
    };
    this.auth.register(resgigterData).subscribe((res) => {
      if (res.status) {
        localStorage.setItem("token", res.data.accessToken);
        localStorage.setItem(
          "email",
          this.registerForm.controls["email"].value
        );
        localStorage.setItem("otp", res.data.otp);
        this.spinner.hide();
        this.toastr.success(res.message);
        this.router.navigateByUrl("verify");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
      this.spinner.hide();
    });
  }
}
