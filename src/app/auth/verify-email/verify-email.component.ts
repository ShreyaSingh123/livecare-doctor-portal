import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { AuthService } from "src/app/auth/auth.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-verify-email",
  templateUrl: "./verify-email.component.html",
  styleUrls: ["./verify-email.component.css"],
})
export class VerifyEmailComponent implements OnInit {
  otp: string;
  email: string;
  data: any;
  data1: any;
  resetForm: FormGroup;
  submitted = false;
  enteredOtp: any;
  isDisabled: boolean;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private route: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.otp = localStorage.getItem("otp");
    this.email = localStorage.getItem("email");
  }

  get f() {
    return this.resetForm.controls;
  }

  onSubmit() {
    var verifyData = {
      otp: this.enteredOtp,
      email: this.email,
    };

    this.spinner.show();
    this.auth.onVerifyEmail(verifyData).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("basic-info");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }
  onOtpChange(otp) {
    if (otp.length == 4) {
      this.enteredOtp = otp;
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }

  onResendOtp() {
    this.spinner.show();
    var data = {
      email: this.email,
    };
    this.auth.resendEmailCode(data).subscribe((res) => {
      if (res.status) {
        this.otp = res.data.otpcode;
        this.spinner.hide();
        this.toastr.success(res.message);
        this.route.navigateByUrl("/verify");
      } else {
        this.spinner.hide();
        this.toastr.error(res.message);
      }
    });
  }
}
