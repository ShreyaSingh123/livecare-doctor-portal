import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-basic-info",
  templateUrl: "./basic-info.component.html",
  styleUrls: ["./basic-info.component.css"],
})
export class BasicInfoComponent implements OnInit {
  basicInfoForm: FormGroup;
  submitted = false;
  options: any = [];
  countries: any;
  nationalities: any;
  states: any;
  specialities: any;
  selected: any;
  constructor(
    private formBuilder: FormBuilder,
    private authServices: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.basicInfoForm = this.formBuilder.group({
      address: ["", Validators.required],
      address2: ["", Validators.required],
      countryId: ["", Validators.required],
      stateId: ["", Validators.required],
      city: ["", Validators.required],
      nationalityId: ["", Validators.required],
      regNo: ["", Validators.required],
      specialityId: ["", Validators.required],
    });
    this.getCountries();
    this.getNationalities();
    this.getSpecility();
  }

  // convenience getter for easy access to form fields
  get basicInfoError() {
    return this.basicInfoForm.controls;
  }
  onEnterAddress() {
    this.basicInfoForm.get("address2").clearValidators(); //clear validation
    this.basicInfoForm.get("address2").setErrors(null); //updating error message
    this.basicInfoForm.updateValueAndValidity(); //update validation
  }
  onEnterAddress2() {
    this.basicInfoForm.get("address").clearValidators(); //clear validation
    this.basicInfoForm.get("address").setErrors(null); //updating error message
    this.basicInfoForm.updateValueAndValidity(); //update validation
  }

  onSubmit() {
    this.spinner.show();
    this.submitted = true;
    if (this.basicInfoForm.invalid) {
      this.spinner.hide();
      return;
    }
    var fullAddress = this.basicInfoForm.controls["address"].value.concat(
      this.basicInfoForm.controls["address2"].value
    );
    var SpecialityData = {
      specialityId: [
        parseInt(this.basicInfoForm.controls["specialityId"].value),
      ],
      nationalityId: parseInt(
        this.basicInfoForm.controls["nationalityId"].value
      ),
      countryId: parseInt(this.basicInfoForm.controls["countryId"].value),
      stateId: parseInt(this.basicInfoForm.controls["stateId"].value),
      city: this.basicInfoForm.controls["city"].value,
      address: fullAddress,
      regNo: this.basicInfoForm.controls["regNo"].value,
    };
    this.authServices
      .addDoctorSpecialityInfo(SpecialityData)
      .subscribe((res) => {
        this.spinner.hide();
        if (res.status) {
          this.router.navigateByUrl("/login");
          this.toaster.success(res.message);
        } else {
          this.toaster.error(res.message);
        }
      });
  }

  getCountries() {
    this.authServices.getCountries().subscribe((res) => {
      this.countries = res.data.list;
      this.spinner.hide();
    });
  }

  //Don't  Remove For future base mutliselect
  // onChange(selection) {
  //   this.selected = selection;
  // }

  getNationalities() {
    this.authServices.getNationalities().subscribe((res) => {
      this.nationalities = res.data.list;
      this.spinner.hide();
    });
  }
  getSpecility() {
    this.authServices.getSpecility().subscribe((res) => {
      this.specialities = res.data.list;

      //Don't  Remove For future base mutliselect
      // this.options = res.data.list.map((person) => ({
      //   id: person.id,
      //   value: person.name,
      // }));
      this.spinner.hide();
    });
  }

  onSelectCountry(countryId: string) {
    this.spinner.show();
    this.authServices.getStates(parseInt(countryId)).subscribe((res) => {
      this.states = res.data.list;
      this.spinner.hide();
    });
  }
}
