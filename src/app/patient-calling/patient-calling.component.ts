import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { PatientCallingService } from "./patient-calling.service";

@Component({
  selector: "app-patient-calling",
  templateUrl: "./patient-calling.component.html",
  styleUrls: ["./patient-calling.component.css"],
})
export class PatientCallingComponent implements OnInit {
  appointments: any;
  timeLeft = 30;
  interval;
  session: OT.Session;
  streams: Array<OT.Stream> = [];
  changeDetectorRef: ChangeDetectorRef;
  constructor(
    private toasterService: ToastrService,
    private ref: ChangeDetectorRef,
    private patientCallingService: PatientCallingService
  ) {
    this.changeDetectorRef = ref;
  }

  ngOnInit() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 30;
      }
    }, 1000);
    this.getOpenTokValues();
    this.patientCallingService
      .initCallSession(
        this.appointments.data.mediaValue.token,
        this.appointments.data.mediaValue.sessionId
      )
      .then((session: OT.Session) => {
        this.session = session;
        this.session.on("streamCreated", (event) => {
          this.streams.push(event.stream);
          this.changeDetectorRef.detectChanges();
        });
        this.session.on("streamDestroyed", (event) => {
          const idx = this.streams.indexOf(event.stream);
          if (idx > -1) {
            this.streams.splice(idx, 1);
            this.changeDetectorRef.detectChanges();
          }
        });
      })
      .then(() => this.patientCallingService.connect())
      .catch((err) => {
        console.error(err);
        alert(
          "Unable to connect. Make sure you have updated  OpenTok details."
        );
      });
  }

  onTurnOff() {
    this.getOpenTokValues();
    this.patientCallingService
      .initCallSession(
        this.appointments.data.mediaValue.token,
        this.appointments.data.mediaValue.sessionId
      )
      .then((session: OT.Session) => {
        this.session = session;
        this.session.on("sessionDisconnected", (event) => {
          if (event.reason === "networkDisconnected") {
            this.toasterService.success("Your network connection terminated");
          } else if (event.reason === "clientDisconnected") {
            this.toasterService.success("Your client connection terminated");
          } else {
            this.toasterService.error("connection terminated");
          }
        });
      })
      .then(() => this.patientCallingService.disconnect())
      .catch((err) => {
        console.error(err);
        alert(
          "Unable to disconnect. Make sure you have updated  OpenTok details."
        );
      });
    // this.router.navigateByUrl("/dashboard/doctor-rating");
  }

  onMute() {
    this.toasterService.success("call is muted");
  }

  getOpenTokValues() {
    this.appointments = JSON.parse(localStorage.getItem("appointments"));
  }
}
