import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PatientCallingRoutingModule } from "./patient-calling-routing.module";
import { PublisherComponent } from "./publisher/publisher.component";
import { SubscriberComponent } from "./subscriber/subscriber.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [PublisherComponent, SubscriberComponent],
  imports: [
    CommonModule,
    PatientCallingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [PublisherComponent, SubscriberComponent],
})
export class PatientCallingModule {}
