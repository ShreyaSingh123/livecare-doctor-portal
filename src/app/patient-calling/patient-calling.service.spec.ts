import { TestBed } from '@angular/core/testing';

import { PatientCallingService } from './patient-calling.service';

describe('PatientCallingService', () => {
  let service: PatientCallingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientCallingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
