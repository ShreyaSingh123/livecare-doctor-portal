import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientCallingComponent } from './patient-calling.component';

describe('PatientCallingComponent', () => {
  let component: PatientCallingComponent;
  let fixture: ComponentFixture<PatientCallingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientCallingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientCallingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
