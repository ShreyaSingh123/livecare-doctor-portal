export * from "./past-appointments/past-appointments.component";
export * from "./pending-appointments/pending-appointments.component";
export * from "./upcoming-appointment/upcoming-appointment.component";
export * from "./view-details/view-details.component";
export * from "./appointment.component";
export * from "./appointment.service";
