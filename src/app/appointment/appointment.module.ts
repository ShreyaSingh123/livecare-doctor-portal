// Angular Inbuilt Package
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Angular Routing and Forms Module
import { AppointmentRoutingModule } from "./appointment-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// Appointments Components
import {
  AppointmentComponent,
  PastAppointmentsComponent,
  PendingAppointmentsComponent,
  UpcomingAppointmentComponent,
  ViewDetailsComponent,
} from ".";

// Modules
import SharedModule from "../shared/shared.module";
import { MaterialModule } from "../shared/main-material/material/material.module";

// Third Party Packages
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { CountdownModule } from "ngx-countdown";

@NgModule({
  declarations: [
    PastAppointmentsComponent,
    UpcomingAppointmentComponent,
    PendingAppointmentsComponent,
    ViewDetailsComponent,
    AppointmentComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    NgxDatatableModule,

    ReactiveFormsModule,
    Ng2SearchPipeModule,
    CountdownModule,
    PaginationModule.forRoot(),
    AppointmentRoutingModule,
  ],
  exports: [PastAppointmentsComponent, UpcomingAppointmentComponent],
  providers: [],
})
export class AppointmentModule {}
