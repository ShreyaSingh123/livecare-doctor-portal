import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { Filter } from "src/app/shared/interfaces/filter";
import { environment } from "src/environments/environment";
import { AppointmentService } from "../appointment.service";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-past-appointments",
  templateUrl: "./past-appointments.component.html",
  styleUrls: ["./past-appointments.component.css"],
})
export class PastAppointmentsComponent implements OnInit {
  list: Array<any> = [];
  isNoDataAvailble: boolean = false;
  totalCount: any;
  currentPage: number = 1;
  searchValue: string;

  pageSize: number = environment.defaultPageSize;
  filters: Filter = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
  };
  characterImg: any;
  isDisbaledButtons = true;
  rootUrl: any;
  pastAppointments: Array<any> = [];
  constructor(
    private appointmentService: AppointmentService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.characterImg = environment.characterImg;
    this.rootUrl = environment.baseImgUrl;
    this.getDoctorpastappointment();
  }

  getDoctorpastappointment() {
    this.spinner.show();
    this.appointmentService
      .getPastAppointments(this.filters)
      .subscribe((res) => {
        this.totalCount = res.data.totalCount;
        if (this.totalCount == 0) {
          this.isNoDataAvailble = true;
        }
        this.pastAppointments = res.data.dataList;
        this.list = res.data.dataList.map(function (user) {
          return {
            appointmentId: user.appointmentId,
            appointmentStatus: user.appointmentStatus,
            date: user.date,
            patientName: user.patientName,
            patientProfilePic: user.patientProfilePic,
            patientId: user.patientId,
            paymentStatus: user.paymentStatus,
            doctorSpecialities: user.doctorSpecialities,
            slotTo: moment
              .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss ")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            slotFrom: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            timeSlotId: user.timeSlotId,
            timer:
              Math.floor(
                new Date(
                  moment
                    .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
                    .local()
                    .format("YYYY-MM-DDTHH:mm:ss")
                ).getTime() - new Date().getTime()
              ) / 1000,
          };
        });

        this.spinner.hide();
      });
  }
  onSearchChange(event: any) {
    if (event == undefined) {
      this.isDisbaledButtons = true;
    } else {
      this.isDisbaledButtons = false;
    }
  }

  pageChanged(event: any): void {
    this.filters.pageNumber = event.page;
    this.getDoctorpastappointment();
    setTimeout(function () {
      var elmnt = document.getElementById("backtotop");
      elmnt.scrollIntoView();
    }, 500);
  }

  onSearch(event) {
    this.isNoDataAvailble = false;
    this.filters.searchQuery = event;
    this.getDoctorpastappointment();
    this.currentPage = 1;
  }
  onClearSearch() {
    this.isDisbaledButtons = true;
    this.isNoDataAvailble = false;
    this.filters.searchQuery = "";
    this.searchValue = "";
    this.currentPage = 1;
    this.getDoctorpastappointment();
  }
}
