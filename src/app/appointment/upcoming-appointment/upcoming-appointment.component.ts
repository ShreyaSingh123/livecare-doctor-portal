import { Component, OnInit } from "@angular/core";
import { AppointmentStatus } from "src/app/shared/enums/appointmentsStatus.enum";
import { FilterWithStatus } from "src/app/shared/interfaces/upcoming-filter";
import { NgxSpinnerService } from "ngx-spinner";
import { DashboardService } from "../../dashboard/dashboard.service";
import * as moment from "moment";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-upcoming-appointment",
  templateUrl: "./upcoming-appointment.component.html",
  styleUrls: ["./upcoming-appointment.component.css"],
})
export class UpcomingAppointmentComponent implements OnInit {
  filters: FilterWithStatus = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
    appointmentStatus: AppointmentStatus.Confirmed,
  };
  upcomingAppointments: [];
  noUpcomingConsults: boolean;
  characterImg: any;
  rootUrl: any;
  appointmentStatus: any = 3;
  constructor(
    private spinner: NgxSpinnerService,
    private dashboardService: DashboardService,
    private router: Router,
    private toster: ToastrService
  ) {}

  ngOnInit(): void {
    localStorage.removeItem("previousData");
    this.getAppointmentList();
    this.rootUrl = environment.baseImgUrl;
  }

  getAppointmentList() {
    this.spinner.show();
    this.dashboardService.getAppointmentList(this.filters).subscribe((res) => {
      if (res.status) {
        if (res.data.dataList == 0) {
          this.noUpcomingConsults = true;
        }
        this.upcomingAppointments = res.data.dataList.map(function (user) {
          return {
            appointmentId: user.appointmentId,
            appointmentStatus: user.appointmentStatus,
            date: user.date,
            patientName: user.patientName,
            patientProfilePic: user.patientProfilePic,
            patientId: user.patientId,
            paymentStatus: user.paymentStatus,
            doctorSpecialities: user.doctorSpecialities,
            slotTo: moment
              .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss ")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            slotFrom: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            timeSlotId: user.timeSlotId,
            timer:
              Math.floor(
                new Date(
                  moment
                    .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
                    .local()
                    .format("YYYY-MM-DDTHH:mm:ss")
                ).getTime() - new Date().getTime()
              ) / 1000,
          };
        });
        this.spinner.hide();
      }
    });
  }

  onViewProfile(details: any) {
    localStorage.setItem("previousData", JSON.stringify(details));
    this.router.navigate(["./upcoming-appointments", details.patientId]);
  }

  deleteAppointment(item) {
    var data = {
      appointmentId: item.appointmentId,
      appointmentStatus: this.appointmentStatus,
    };

    Swal.fire({
      title: "Appointment Request",
      html: "Are you sure you want to delete this appointment",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();

        this.dashboardService.UpdateAppointmentStatus(data).subscribe((res) => {
          this.getAppointmentList();
          this.spinner.hide();

          if (res.status == true) {
            this.toster.success(res.message);
          } else {
            this.toster.warning(res.message);
          }
        });
      }
    });
  }
}
