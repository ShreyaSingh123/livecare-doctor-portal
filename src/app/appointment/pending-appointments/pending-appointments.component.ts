import { Component, OnInit } from "@angular/core";
import { FilterWithStatus } from "src/app/shared/interfaces/upcoming-filter";
import { NgxSpinnerService } from "ngx-spinner";
import { DashboardService } from "../../dashboard/dashboard.service";
import * as moment from "moment";
import { environment } from "src/environments/environment";
import { AppointmentStatus } from "src/app/shared/enums/appointmentsStatus.enum";
import { Router } from "@angular/router";
import { AppointmentService } from "../appointment.service";
@Component({
  selector: "app-pending-appointments",
  templateUrl: "./pending-appointments.component.html",
  styleUrls: ["./pending-appointments.component.css"],
})
export class PendingAppointmentsComponent implements OnInit {
  filters: FilterWithStatus = {
    sortOrder: "",
    sortField: "",
    pageNumber: 1,
    pageSize: 10,
    searchQuery: "",
    filterBy: "",
    appointmentStatus: AppointmentStatus.Pending,
  };
  pendingAppointments: [];
  characterImg: any;
  rootUrl: any;
  noData: boolean = false;
  constructor(
    private spinner: NgxSpinnerService,
    private dashboardService: DashboardService,
    private appointmentService: AppointmentService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAppointmentList();
    this.rootUrl = environment.baseImgUrl;
  }

  getAppointmentList() {
    this.spinner.show();
    this.dashboardService.getAppointmentList(this.filters).subscribe((res) => {
      if (res.status) {
        if (res.data.dataList.length == 0) {
          this.noData = true;
        }
        this.pendingAppointments = res.data.dataList.map(function (user) {
          return {
            appointmentId: user.appointmentId,
            appointmentStatus: user.appointmentStatus,
            date: user.date,
            patientName: user.patientName,
            patientProfilePic: user.patientProfilePic,
            patientId: user.patientId,
            paymentStatus: user.paymentStatus,
            doctorSpecialities: user.doctorSpecialities,
            slotTo: moment
              .utc(user.slotTo, "DD-MM-YYYY HH:mm:ss ")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            slotFrom: moment
              .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            timeSlotId: user.timeSlotId,
            timer:
              Math.floor(
                new Date(
                  moment
                    .utc(user.slotFrom, "DD-MM-YYYY HH:mm:ss")
                    .local()
                    .format("YYYY-MM-DDTHH:mm:ss")
                ).getTime() - new Date().getTime()
              ) / 1000,
          };
        });
        this.spinner.hide();
      }
    });
  }
  onViewDetails(details: any) {
    localStorage.setItem("previousData", JSON.stringify(details));
    this.router.navigate(["./pending-appointments", details.patientId]);
  }
}
