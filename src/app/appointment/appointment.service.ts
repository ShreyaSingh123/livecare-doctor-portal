import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";
import { map } from "rxjs/operators";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AppointmentService {
  constructor(private http: HttpClient) {}

  // getPastAppointments
  getPastAppointments(data: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getDoctorPastAppointments +
          "?pageNumber=" +
          data.pageNumber +
          "&pageSize=" +
          data.pageSize +
          "&sortOrder=" +
          data.sortOrder +
          "&sortField=" +
          data.sortField +
          "&searchQuery=" +
          data.searchQuery +
          "&filterBy=" +
          data.filterBy
      )
      .pipe(
        map((res: any) => {
          return res;
        })
      );
  }

  //GetPatientInfoById
  getPatientInfoById(PatientId: any) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getPatientInfoById +
        "?PatientId=" +
        PatientId
    );
  }

  //updateAppointmentStatus
  updateAppointmentStatus(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateAppointmentStatus,
      data
    );
  }
}
