import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//Components
import {
  PastAppointmentsComponent,
  UpcomingAppointmentComponent,
  PendingAppointmentsComponent,
  ViewDetailsComponent,
} from ".";

//Shared
import { AuthGuard } from "../shared/guards/auth.guard";
import { AppointmentComponent } from "./appointment.component";

const routes: Routes = [
  {
    path: "",
    component: AppointmentComponent,
    children: [
      {
        path: "past-appointments",
        component: PastAppointmentsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "upcoming-appointments",
        component: UpcomingAppointmentComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "upcoming-appointments/:id",
        component: ViewDetailsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "pending-appointments",
        component: PendingAppointmentsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: "pending-appointments/:id",
        component: ViewDetailsComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppointmentRoutingModule {}
