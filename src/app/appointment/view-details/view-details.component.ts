import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppointmentService } from "../appointment.service";
import { NgxSpinnerService } from "ngx-spinner";
import { environment } from "src/environments/environment";
import { Location } from "@angular/common";
import { ToastrService } from "ngx-toastr";
import * as moment from "moment";
@Component({
  selector: "app-view-details",
  templateUrl: "./view-details.component.html",
  styleUrls: ["./view-details.component.css"],
})
export class ViewDetailsComponent implements OnInit {
  patientId: any;
  patientInfo: any;
  isNoDataAvailble: boolean = false;
  pendingAppointments: [];
  previousData: any;
  characterImg: any;
  rootUrl: any;
  dateOfBirth: any;
  age: any;
  isShowButtons: boolean = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private appointmentService: AppointmentService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private toaster: ToastrService
  ) {
    var currentUrl = this.router.url;
    this.isShowButtons = currentUrl.includes("/upcoming-appointments");
    this.previousData = JSON.parse(localStorage.getItem("previousData"));
    this.rootUrl = environment.baseImgUrl;
    this.patientId = this.activatedRoute.snapshot.params;
    this.getPatientInfo();
  }

  ngOnInit(): void {}

  getPatientInfo() {
    this.spinner.show();
    this.appointmentService
      .getPatientInfoById(this.patientId.id)
      .subscribe((res) => {
        this.spinner.hide();
        this.patientInfo = res.data;
        this.dateOfBirth = res.data.dateOfBirth;
        var timeDiff = Math.abs(
          Date.now() -
            new Date(
              moment(this.dateOfBirth, "DD-MM-YYYY").format("MM-DD-YYYY")
            ).getTime()
        );
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      });
  }

  acceptConfirmation(appointmentId: any) {
    this.spinner.show();
    var dataForUpdateStaus = {
      appointmentId: appointmentId,
      appointmentStatus: 1,
    };
    this.appointmentService
      .updateAppointmentStatus(dataForUpdateStaus)
      .subscribe((res) => {
        this.spinner.hide();
        if (res.status) {
          this.router.navigateByUrl("upcoming-appointments");
          this.toaster.success(res.message);
        } else {
          this.toaster.error(res.message);
          return;
        }
      });
  }

  cancelConfirmation(appointmentId: any) {
    this.spinner.show();
    var dataForUpdateStaus = {
      appointmentId: appointmentId,
      appointmentStatus: 2,
    };
    this.appointmentService
      .updateAppointmentStatus(dataForUpdateStaus)
      .subscribe((res) => {
        this.spinner.hide();
        if (res.status) {
          this.router.navigateByUrl("pending-appointments");
          this.toaster.success(res.message);
        } else {
          this.toaster.error(res.message);
          return;
        }
      });
  }
}
