import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MailComponent } from "./mail/mail.component";
import { BillingComponent } from "./billing/billing.component";
import { ToDoListComponent } from "./to-do-list/to-do-list.component";
import { EarningReportsComponent } from "./earning-reports/earning-reports.component";

import { AuthGuard } from "../shared/guards/auth.guard";

const routes: Routes = [
  {
    path: "mail",
    component: MailComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "billing",
    component: BillingComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "to-do-list",
    component: ToDoListComponent,
    canActivate: [AuthGuard],
  },

  {
    path: "earning",
    component: EarningReportsComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule],
})
export class ComponentsRoutingModule {}
