import { Component, OnInit } from "@angular/core";
import { ViewEncapsulation } from "@angular/core";
import { MatCalendarCellCssClasses } from "@angular/material/datepicker";
import Swal from "sweetalert2";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-mail",
  templateUrl: "./mail.component.html",
  styleUrls: ["./mail.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class MailComponent implements OnInit {
  number = 123;

  // Reactive Form
  formGroup = new FormGroup({
    number: new FormControl(1234),
  });

  ngOnInit() {}

  dateClass = (d: Date): MatCalendarCellCssClasses => {
    const date = d.getDate();

    // Highlight the 7th and 20th day of each month.
    return date == 4 || date == 9 ? "example-custom-date-class" : "";
  };

  constructor() {}

  show() {
    Swal.fire("Any fool can use a computer");
  }
}
