import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ComponentsRoutingModule } from "./components-routing.module";
import { MailComponent } from "./mail/mail.component";
import { MaterialModule } from "../shared/main-material/material/material.module";
import { ToDoListComponent } from "./to-do-list/to-do-list.component";
import { BillingComponent } from "./billing/billing.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EarningReportsComponent } from "./earning-reports/earning-reports.component";

import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { PaginationModule } from "ngx-bootstrap/pagination";
import SharedModule from "../shared/shared.module";
import { IntlInputPhoneModule } from "intl-input-phone";
import { Ng2SearchPipeModule } from "ng2-search-filter";
@NgModule({
  declarations: [
    MailComponent,
    ToDoListComponent,
    BillingComponent,

    EarningReportsComponent,
  ],

  imports: [
    CommonModule,
    SharedModule,
    IntlInputPhoneModule,
    ComponentsRoutingModule,
    MaterialModule,
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    PaginationModule.forRoot(),
  ],
  exports: [],
})
export class ComponentsModule {}
