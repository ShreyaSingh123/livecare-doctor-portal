import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-to-do-list",
  templateUrl: "./to-do-list.component.html",
  styleUrls: ["./to-do-list.component.css"],
})
export class ToDoListComponent implements OnInit {
  rows = [];

  temp = [];

  columns = [{ prop: "name" }, { prop: "Company" }, { prop: "Gender" }];

  ColumnMode;
  table: any;

  constructor() {
    this.fetch((data) => {
      // cache our list
      this.temp = [...data];

      // push our inital complete list
      this.rows = data;
    });
  }
  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open("GET", `assets/data/company.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  public myFilter = (d: Date): boolean => {
    const date = d.getDate();
    // Prevent Saturday and Sunday from being selected.
    return date !== 4 && date !== 9 && date !== 12;
  };

  ngOnInit(): void {}
}
