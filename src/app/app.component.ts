import { Component } from "@angular/core";
declare var $: any;
import * as AOS from "aos";
import { NgxSpinnerService } from "ngx-spinner";
import { Router, NavigationStart } from "@angular/router";
import { AuthService } from "./auth/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "LiveCare24";
  hiddenHeaderAside = false;
  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    public auth: AuthService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 3000);

    AOS.init();
    $(".flip").click(function (e) {
      $(".panel , .body-content").toggleClass("open");
    });
  }
}
