import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { DashboardService } from "src/app/dashboard/dashboard.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  upcomingAppointments: any;
  profileDisplay: boolean = false;

  dashboardData: any;
  percentageProfileComplete: any;
  specialityInfo: any;

  baseImgUrl = environment.baseImgUrl;
  constructor(
    private dashboardService: DashboardService,
    private spinner: NgxSpinnerService
  ) {
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
      window.history.pushState(null, "", window.location.href);
    };
  }

  ngOnInit() {
    this.getDoctorDashboardInfo();
  }

  getDoctorDashboardInfo() {
    this.spinner.show();
    this.dashboardService.getDoctorDashboardInfo().subscribe((res: any) => {
      this.dashboardData = res.data;

      this.specialityInfo = this.dashboardData.doctorSpecialities;
      this.percentageProfileComplete = res.data.percentageProfileComplete;
      if (this.percentageProfileComplete <= 100) {
        this.profileDisplay = true;
      }
      this.spinner.hide();
    });
  }
}
