import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  // getAllAppointmentList
  getAppointmentList(data: any) {
    return this.http
      .get<any>(
        environment.apiUrl +
          "/" +
          ApiEndPoint.getUpcomingAppointment +
          "?pageNumber=" +
          data.pageNumber +
          "&pageSize=" +
          data.pageSize +
          "&sortOrder=" +
          data.sortOrder +
          "&sortField=" +
          data.sortField +
          "&searchQuery=" +
          data.searchQuery +
          "&filterBy=" +
          data.filterBy +
          "&appointmentStatus=" +
          data.appointmentStatus
      )
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }

  getDoctorDashboardInfo(): Observable<any> {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDoctorDashboardInfo
    );
  }

  UpdateAppointmentStatus(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateAppointmentStatus,
      data
    );
  }
}
