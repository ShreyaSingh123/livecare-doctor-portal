import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DashboardComponent } from ".";
import { MaterialModule } from "../shared/main-material/material/material.module";

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,

    DashboardRoutingModule,
  ],
  exports: [DashboardComponent],
})
export class DashboardModule {}
