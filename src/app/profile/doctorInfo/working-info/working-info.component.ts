import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { NgxSpinnerService } from "ngx-spinner";
import { NgxSpinner } from "ngx-spinner/lib/ngx-spinner.enum";
import { ToastrService } from "ngx-toastr";
import { ProfileService } from "../../profile.service";

@Component({
  selector: "app-working-info",
  templateUrl: "./working-info.component.html",
  styleUrls: ["./working-info.component.css"],
})
export class WorkingInfoComponent implements OnInit {
  @ViewChild("appointmentDailog", { static: true })
  appointmentDailog: TemplateRef<any>;

  @ViewChild("specialityDailog", { static: true })
  specialityDailog: TemplateRef<any>;

  @ViewChild("medicalDailog", { static: true })
  medicalDailog: TemplateRef<any>;

  appointmentPriceForm: FormGroup;
  submitted = false;
  degreeList: any = [];
  workInfo: any;
  specialityList: any = [];
  updateMedicalDetailsForm: FormGroup;
  updateSpecialityForm: FormGroup;
  doctorEducation: Array<any> = [];

  constructor(
    private formBuilder: FormBuilder,
    private profileService: ProfileService,
    public dialog: MatDialog,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit() {
    this.appointmentPriceForm = this.formBuilder.group({
      appointmentFee: [, Validators.required],
    });

    this.updateMedicalDetailsForm = this.formBuilder.group({
      degree: [, Validators.required],
      otherDegreeInstituteName: [, Validators.required],
      degreeInstituteName: [, Validators.required],
      otherDegree: [, Validators.required],
    });

    this.updateSpecialityForm = this.formBuilder.group({
      speciality: [, Validators.required],
      experienceMonth: [, Validators.required],
      experienceYear: [, Validators.required],
    });

    this.getDegrees();
    this.getDoctorWorkInfo();
    this.getSpeciality();
  }
  get f() {
    return this.appointmentPriceForm.controls;
  }

  checkLength2(e, input) {
    const functionalKeys = ["Backspace", "ArrowRight", "ArrowLeft"];

    if (functionalKeys.indexOf(e.key) !== -1) {
      return;
    }

    const keyValue = +e.key;
    if (isNaN(keyValue)) {
      e.preventDefault();
      return;
    }

    const hasSelection =
      input.selectionStart !== input.selectionEnd &&
      input.selectionStart !== null;
    let newValue;
    if (hasSelection) {
      newValue = this.replaceSelection(input, e.key);
    } else {
      newValue = input.value + keyValue.toString();
    }

    if (+newValue > 11 || newValue.length > 2) {
      e.preventDefault();
    }
  }
  private replaceSelection(input, key) {
    const inputValue = input.value;
    const start = input.selectionStart;
    const end = input.selectionEnd || input.selectionStart;
    return inputValue.substring(0, start) + key + inputValue.substring(end + 1);
  }

  getDoctorWorkInfo() {
    this.profileService.getDoctorWorkInfo().subscribe((res) => {
      this.workInfo = res.data;
      this.doctorEducation = res.data.doctorEducation;
    });
  }

  getDegrees() {
    this.profileService.getDegrees().subscribe((res) => {
      this.degreeList = res.data.list;
    });
  }

  getSpeciality() {
    this.profileService.getDoctorSpeciality().subscribe((res) => {
      this.specialityList = res.data.list;
    });
  }

  updateAppointmentPrice() {
    this.appointmentPriceForm.controls["appointmentFee"].setValue(
      this.workInfo.appointmentFees
    );
    this.dialog.open(this.appointmentDailog, {
      disableClose: true,
      height: "300px",
      width: "600px",
    });
  }

  onUpdateAppointment() {
    this.submitted = true;

    var data = {
      fee: parseInt(this.appointmentPriceForm.value.appointmentFee),
    };
    if (this.appointmentPriceForm.invalid) {
      return;
    }
    this.spinner.show();
    this.profileService.addAppointmentFee(data).subscribe((res) => {
      if (res.status) {
        this.dialog.closeAll();
        this.spinner.hide();
        this.toaster.success(res.message);
        this.getDoctorWorkInfo();
      }
    });
  }

  openMedicalModal() {
    this.dialog.open(this.medicalDailog, {
      disableClose: true,
      height: "400px",
      width: "600px",
    });
  }

  updateMedicalDetails() {
    if (this.updateMedicalDetailsForm.get("otherDegree").value == null) {
      var data = {
        arrDoctorEducationalViewModel: [
          {
            degreeId: parseInt(
              this.updateMedicalDetailsForm.get("degree").value
            ),
            instituteName: this.updateMedicalDetailsForm.get(
              "degreeInstituteName"
            ).value,
          },
        ],
      };
    } else {
      var data = {
        arrDoctorEducationalViewModel: [
          {
            degreeId: parseInt(
              this.updateMedicalDetailsForm.get("degree").value
            ),
            instituteName: this.updateMedicalDetailsForm.get(
              "degreeInstituteName"
            ).value,
          },
          {
            degreeId: parseInt(
              this.updateMedicalDetailsForm.get("otherDegree").value
            ),
            instituteName: this.updateMedicalDetailsForm.get(
              "otherDegreeInstituteName"
            ).value,
          },
        ],
      };
    }
    this.profileService.updateDoctorEducation(data).subscribe((res) => {
      if (res.status) {
        this.dialog.closeAll();
        this.spinner.hide();
        this.toaster.success(res.message);
        this.getDoctorWorkInfo();
      }
    });
  }

  updateSpeciality() {
    this.dialog.open(this.specialityDailog, {
      disableClose: true,
      height: "300px",
      width: "600px",
    });
  }

  updateSpecialityDetails() {
    var data = {
      specialityId: [
        parseInt(this.updateSpecialityForm.get("speciality").value),
      ],
      experience: parseFloat(
        this.updateSpecialityForm.get("experienceYear").value +
          "." +
          this.updateSpecialityForm.get("experienceMonth").value
      ),
    };
    this.profileService.updateDoctorSpeciality(data).subscribe((res) => {
      if (res.status) {
        this.dialog.closeAll();
        this.spinner.hide();
        this.toaster.success(res.message);
        this.getDoctorWorkInfo();
      }
    });
  }
}
