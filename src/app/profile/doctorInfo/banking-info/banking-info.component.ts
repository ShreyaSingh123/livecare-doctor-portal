import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ProfileService } from "../../profile.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-banking-info",
  templateUrl: "./banking-info.component.html",
  styleUrls: ["./banking-info.component.css"],
})
export class BankingInfoComponent implements OnInit {
  @ViewChild("bankInfoDailog", { static: true })
  bankInfoDailog: TemplateRef<any>;
  doctorBankInfo: any = [];
  bankInfoForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private profileService: ProfileService,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.getDoctorBankInfo();

    this.bankInfoForm = this.formBuilder.group({
      bankName: ["", Validators.required],
      accountNumber: ["", Validators.required],
      routeNo: ["", Validators.required],
      branchCode: ["", Validators.required],
      postCode: ["", Validators.required],
      address: ["", Validators.required],
    });
  }
  get error() {
    return this.bankInfoForm.controls;
  }

  updateBankInfo() {
    this.bankInfoForm.controls["bankName"].setValue(
      this.doctorBankInfo.bankName
    );
    this.bankInfoForm.controls["accountNumber"].setValue(
      this.doctorBankInfo.accountNumber
    );
    this.bankInfoForm.controls["routeNo"].setValue(this.doctorBankInfo.routeNo);
    this.bankInfoForm.controls["branchCode"].setValue(
      this.doctorBankInfo.branchCode
    );
    this.bankInfoForm.controls["postCode"].setValue(
      this.doctorBankInfo.postCode
    );
    this.bankInfoForm.controls["address"].setValue(this.doctorBankInfo.address);
    this.dialog.open(this.bankInfoDailog, {
      disableClose: true,
      height: "530px",
      width: "800px",
      backdropClass: "backdropBackground",
    });
  }

  getDoctorBankInfo() {
    this.spinner.show();
    this.profileService.getDoctorBankInfo().subscribe((res) => {
      this.doctorBankInfo = res.data;
      this.spinner.hide();
    });
  }
  onUpdateBankInfo() {
    this.submitted = true;
    if (this.bankInfoForm.invalid) {
      return;
    }

    var data = {
      bankName: this.bankInfoForm.controls["bankName"].value,
      accountNumber: parseInt(
        this.bankInfoForm.controls["accountNumber"].value
      ),
      routeNo: this.bankInfoForm.controls["routeNo"].value,
      branchCode: this.bankInfoForm.controls["branchCode"].value,
      postCode: this.bankInfoForm.controls["postCode"].value,
      address: this.bankInfoForm.controls["address"].value,
    };
    this.profileService.addBankInfo(data).subscribe((res) => {
      if (res.status) {
        this.toaster.success(res.message);
        this.dialog.closeAll();
        this.getDoctorBankInfo();
      } else {
        this.toaster.success(res.message);
        return;
      }
    });
  }
}
