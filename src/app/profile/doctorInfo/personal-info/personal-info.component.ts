import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ProfileService } from "../../profile.service";
import { NgxSpinnerService } from "ngx-spinner";
import { MatDialog } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { AuthService } from "../../../auth/auth.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import {
  ConfigurationOptions,
  ContentOptionsEnum,
  OutputOptionsEnum,
  SortOrderEnum,
} from "intl-input-phone";
@Component({
  selector: "app-personal-info",
  templateUrl: "./personal-info.component.html",
  styleUrls: ["./personal-info.component.css"],
})
export class PersonalInfoComponent implements OnInit {
  @ViewChild("aboutDailog", { static: true }) aboutDailog: TemplateRef<any>;
  @ViewChild("addressDailog", { static: true }) addressDailog: TemplateRef<any>;
  @ViewChild("phoneDailog", { static: true }) phoneDailog: TemplateRef<any>;
  @ViewChild("languageDailog", { static: true })
  languageDailog: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  selectedStateId: any;
  personalInfo: any;
  personalIndoData: any;
  aboutUpdateForm: FormGroup;
  addressUpdateForm: FormGroup;
  languageForm: FormGroup;
  submitted = false;
  countries: any;
  stateList: any;
  selectedCountryId: any;
  nationalities: [];
  nationalityId: any;
  selectedItems = [];
  dropdownList = [];
  OutputValue2: any;
  requiredDailCode: string = "";
  configOption: ConfigurationOptions;
  doctorBankInfo: any;
  dropdownSettings: IDropdownSettings;
  phoneForm: FormGroup;
  phoneNumber: any;
  dialCode: any;
  constructor(
    private profileService: ProfileService,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private toaster: ToastrService,
    private authService: AuthService
  ) {
    this.configOption = new ConfigurationOptions();
    this.configOption.SelectorClass = "OptionType3";
    this.configOption.SortBy = SortOrderEnum.CountryName;
    this.configOption.OptionTextTypes = [];
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.Flag);
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.CountryName);
    this.configOption.OptionTextTypes.push(ContentOptionsEnum.CountryPhoneCode);
    this.configOption.OutputFormat = OutputOptionsEnum.Number;
  }
  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: "id",
      textField: "name",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };
    this.getDoctorInfo();
    this.getCountries();
    this.getNationalities();
    this.getLanguages();

    this.aboutUpdateForm = this.formBuilder.group({
      about: [, Validators.required],
    });
    this.addressUpdateForm = this.formBuilder.group({
      address: [Validators.required],
      address2: [],
      country: ["", Validators.required],
      state: ["", Validators.required],
      city: ["", Validators.required],
      nationality: ["", Validators.required],
    });
    this.languageForm = this.formBuilder.group({
      language: ["", Validators.required],
    });
    this.phoneForm = this.formBuilder.group({
      phone: ["", Validators.required],
    });
  }

  getDoctorInfo() {
    this.spinner.show();
    this.profileService.getDoctorPersonalInfo().subscribe((res) => {
      this.personalInfo = res.data;
      this.phoneNumber = res.data.phoneNumber;
      if (res.data.dialCode.length === 4) {
        this.dialCode =
          res.data.dialCode.slice(0, 1) +
          " " +
          "(" +
          res.data.dialCode.slice(1) +
          ")";
      } else {
        this.dialCode = res.data.dialCode;
      }

      this.spinner.hide();
    });
  }

  get error() {
    return this.aboutUpdateForm.controls;
  }

  getCountries() {
    this.authService.getCountries().subscribe((res) => {
      this.countries = res.data.list;
    });
  }
  onSelectCountry(id: any) {
    this.selectedCountryId = parseInt(id);
    this.getstatebyCountries(this.selectedCountryId);
  }
  getstatebyCountries(selectedCountryId: any) {
    this.spinner.show();
    this.authService.getStates(selectedCountryId).subscribe((res) => {
      this.spinner.hide();
      this.stateList = res.data.list;
    });
  }
  onSelectNationality(id: any) {
    this.nationalityId = id;
  }

  openAboutModal() {
    this.aboutUpdateForm.controls["about"].setValue(this.personalInfo.about);
    this.dialog.open(this.aboutDailog, { disableClose: true });
  }
  openAddressModal() {
    this.addressUpdateForm.controls["address"].setValue(
      this.personalInfo.currentAddress
    );
    this.addressUpdateForm.controls["address2"].setValue("");
    this.addressUpdateForm.controls["country"].setValue(
      this.personalInfo.countryId
    );

    this.addressUpdateForm.controls["state"].setValue(
      this.personalInfo.stateId
    );

    this.addressUpdateForm.controls["city"].setValue(this.personalInfo.city);
    this.addressUpdateForm.controls["nationality"].setValue(
      this.personalInfo.nationalityId
    );
    this.dialog.open(this.addressDailog, { disableClose: true });
  }

  openLanguageModal() {
    this.selectedItems = this.personalInfo.doctorLangauges;
    this.dialog.open(this.languageDailog, {
      disableClose: true,
      height: "400px",
      width: "600px",
    });
  }

  onUpdateAbout() {
    this.submitted = true;
    if (this.aboutUpdateForm.invalid) {
      return;
    }
    this.profileService
      .updateDoctorBasicInfo(this.aboutUpdateForm.value.about)
      .subscribe((res) => {
        if (res.status) {
          this.getDoctorInfo();
          this.toaster.success(res.message);
          this.dialog.closeAll();
        } else {
          this.toaster.error(res.message);
          this.dialog.closeAll();
        }
      });
  }

  onSelectState(id: any) {
    this.selectedStateId = id;
  }
  onUpdateAddress() {
    var data = {
      countryId: parseInt(this.selectedCountryId),
      stateId: parseInt(this.selectedStateId),
      city: this.addressUpdateForm.controls["city"].value,
      nationalityId: parseInt(this.nationalityId),
      currentAddress: this.addressUpdateForm.controls["address"].value,
    };

    this.profileService.updateUserAddress(data).subscribe((res) => {
      this.getDoctorInfo();
    });
  }

  onUpdateNationality() {
    this.profileService
      .updateNationalityInfo(this.nationalityId)
      .subscribe((res) => {
        if (res.status) {
          this.toaster.success(res.message);
        } else {
          this.toaster.error(res.message);
        }
      });
    this.getDoctorInfo();
  }

  getNationalities() {
    this.authService.getNationalities().subscribe((res) => {
      this.nationalities = res.data.list;
    });
  }

  getLanguages() {
    this.profileService.getLanguages().subscribe((res) => {
      this.dropdownList = res.data.list;
    });
  }

  addLanguage() {
    var data = {
      languageId: this.languageForm.controls["language"].value.map((x) => x.id),
    };
    this.profileService.addLanguage(data).subscribe((res) => {
      if (res.status) {
        this.toaster.success(res.message);
        this.getDoctorInfo();
      } else {
        this.toaster.error(res.message);
        this.getDoctorInfo();
      }
    });
  }

  openPhoneModal() {
    this.requiredDailCode = "+" + this.dialCode + " " + this.phoneNumber;
    this.dialog.open(this.phoneDailog, {
      disableClose: true,
      height: "300px",
      width: "600px",
    });
  }
  onEnterNumber(outputResult) {
    this.OutputValue2 = outputResult;
  }

  updatePhoneNumber() {
    var data = {
      phoneNumber: this.OutputValue2.Number,
      dialCode: this.OutputValue2.CountryModel.CountryPhoneCode.replace(
        /[^A-Z0-9]/gi,
        ""
      ),
    };
    this.profileService.updatePhoneNumber(data).subscribe((res) => {
      this.dialog.closeAll();
      this.getDoctorInfo();
    });
  }
}
