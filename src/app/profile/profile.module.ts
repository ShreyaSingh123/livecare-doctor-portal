import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

//Routing and Forms Module
import { ProfileRoutingModule } from "./profile-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

//Third Party Module
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { IntlInputPhoneModule } from "intl-input-phone";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { MaterialModule } from "../shared/main-material/material/material.module";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
// Profile Componets
import {
  BankingInfoComponent,
  MyProfileComponent,
  PersonalInfoComponent,
  WorkingInfoComponent,
} from ".";

//Shared
import SharedModule from "../shared/shared.module";
import { UpdateModalComponent } from "./update-modal/update-modal.component";
import { MAT_DIALOG_DEFAULT_OPTIONS } from "@angular/material/dialog";

@NgModule({
  declarations: [
    MyProfileComponent,
    PersonalInfoComponent,
    BankingInfoComponent,
    WorkingInfoComponent,
    UpdateModalComponent,
  ],
  imports: [
    CommonModule,

    IntlInputPhoneModule,
    MaterialModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    PaginationModule.forRoot(),
    ProfileRoutingModule,
    SharedModule,
  ],
  exports: [
    MyProfileComponent,
    PersonalInfoComponent,
    BankingInfoComponent,
    WorkingInfoComponent,
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } },
  ],
})
export class ProfileModule {}
