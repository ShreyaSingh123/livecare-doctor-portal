import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";
import { env } from "process";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  constructor(private http: HttpClient) {}

  //get Doctor  personal info
  getDoctorPersonalInfo(): Observable<any> {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.personalInfo
    );
  }

  //update Doctor  personal info
  updateDoctorBasicInfo(data: any): Observable<any> {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateDoctorBasicInfo,
      { about: data }
    );
  }
  updateNationalityInfo(data: any): Observable<any> {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateDoctorBasicInfo,
      { nationality: data }
    );
  }
  updateUserAddress(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateUserAddress,
      data
    );
  }

  getLanguages() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getLanguages
    );
  }

  addLanguage(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.addLanguage,
      data
    );
  }

  addBankInfo(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.addBankInfo,
      data
    );
  }

  getDoctorBankInfo() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDoctorBankInfo
    );
  }
  updatePhoneNumber(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updatePhoneNumber,
      data
    );
  }
  getDoctorWorkInfo() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDoctorWorkInfo
    );
  }

  getDegrees() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDegrees
    );
  }

  addAppointmentFee(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateAppointmentFee,
      data
    );
  }

  //Update doctor eduction
  updateDoctorEducation(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateDoctorEducation,
      data
    );
  }

  //get Doctor speciality
  getDoctorSpeciality() {
    return this.http.get<any>(
      environment.apiUrl + "/" + ApiEndPoint.getDoctorSpeciality
    );
  }

  // post Doctor speciality
  updateDoctorSpeciality(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.updateSpeciality,
      data
    );
  }
}
