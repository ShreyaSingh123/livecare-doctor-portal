export * from "./doctorInfo/banking-info/banking-info.component";
export * from "./doctorInfo/personal-info/personal-info.component";
export * from "./doctorInfo/working-info/working-info.component";
export * from "./my-profile/my-profile.component";
