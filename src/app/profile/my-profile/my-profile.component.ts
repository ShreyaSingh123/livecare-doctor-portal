import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/auth/auth.service";
import { environment } from "src/environments/environment";
import { DashboardService } from "../../dashboard/dashboard.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ProfileService } from "../profile.service";

@Component({
  selector: "app-my-profile",
  templateUrl: "./my-profile.component.html",
  styleUrls: ["./my-profile.component.css"],
})
export class MyProfileComponent {
  _fileData: File = null;
  profilePic: string;
  viewData: any;
  url: any;
  doctorInfo: any;
  dashboardInfo: any;
  baseImgUrl = environment.baseImgUrl;

  constructor(
    private auth: AuthService,
    private dashboardService: DashboardService,

    private spinner: NgxSpinnerService
  ) {}
  ngOnInit() {
    this.getDoctorDashboardInfo();
  }

  getDoctorDashboardInfo() {
    this.spinner.show();
    this.dashboardService.getDoctorDashboardInfo().subscribe((res: any) => {
      if (res.data.profilePic != null) {
        this.url = res.data.profilePic;
        this.dashboardInfo = res.data;
        this.getProfilePic();
      }

      this.spinner.hide();
    });
  }

  changeProfilePic(fileInput: any) {
    this._fileData = <File>fileInput.target.files[0];
    this.onUpdateProfilePic();
  }
  onUpdateProfilePic() {
    const formData = new FormData();
    formData.append("imgFile", this._fileData, this._fileData.name);
    this.auth.updateProfilePic(formData).subscribe((res) => {
      if (res.status) {
        localStorage.setItem("profilePic", res.data.profilepicurl);
        this.profilePic =
          environment.baseImgUrl + localStorage.getItem("profilePic");
      }
    });
  }

  showProfilePic(src: any) {
    this.profilePic = src;
  }
  getProfilePic() {
    this.profilePic = environment.baseImgUrl + this.url;
  }
}
