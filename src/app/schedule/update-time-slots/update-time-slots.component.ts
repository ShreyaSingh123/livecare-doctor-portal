import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { ScheduleService } from "../schedule.service";

@Component({
  selector: "app-update-time-slots",
  templateUrl: "./update-time-slots.component.html",
  styleUrls: ["./update-time-slots.component.css"],
})
export class UpdateTimeSlotsComponent implements OnInit {
  slotsByDate: any;
  timeShift: any;
  todayDate: any;
  todayTimeShift: Array<any> = [];
  updateTimeSlots: any;
  constructor(
    private scheduleSrvice: ScheduleService,
    private spinner: NgxSpinnerService,
    private toster: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.todayDate = localStorage.getItem("Date");
    this.slotsByDate = moment(this.todayDate, "DD-MM-YYYY").format(
      "MM-DD-YYYY"
    );
    this.onTodayShift();
  }

  onTodayShift() {
    this.scheduleSrvice

      .getDoctorDateWiseTimeShift(this.todayDate)
      .subscribe((res) => {
        this.timeShift = res.data.map((x) => {
          return {
            id: x.id,
            fromTime: moment
              .utc(x.fromTime, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
            toTime: moment
              .utc(x.toTime, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("MM-DD-YYYY HH:mm:ss"),
          };
        });
        this.todayTimeShift = this.timeShift.filter((x) =>
          x.fromTime.includes(this.slotsByDate)
        );
      });
  }
  onDeleteSlots(id: any) {
    Swal.fire({
      title: "Delete",
      html:
        "This Action will not delete any existing consultation appointments",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.scheduleSrvice.removeDoctorSchedule(id).subscribe((res) => {
          this.onTodayShift();
          this.spinner.hide();

          if (res.status == true) {
            this.toster.success(res.message);
          } else {
            this.toster.warning(res.message);
          }
        });
      }
    });
  }

  onUpdateSlots(updateData) {

    this.updateTimeSlots = {
      scheduleDate: moment(updateData.fromTime, "MM-DD-YYYY HH:mm:ss").format(
        "MM-DD-YYYY"
      ),
      fromTime: moment(updateData.fromTime, "MM-DD-YYYY HH:mm:ss").format(
        "HH:mm"
      ),
      id: updateData.id,
      toTime: moment(updateData.toTime, "MM-DD-YYYY HH:mm:ss").format("HH:mm"),
    };
    this.updateTimeSlots.roll = "update";
    localStorage.setItem(
      "updateTimeSlots",
      JSON.stringify(this.updateTimeSlots)
    );

    this.router.navigateByUrl("/my-schedule");
  }
  ngOnDestroy() {
    localStorage.removeItem("Date");
  }
}
