import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTimeSlotsComponent } from './update-time-slots.component';

describe('UpdateTimeSlotsComponent', () => {
  let component: UpdateTimeSlotsComponent;
  let fixture: ComponentFixture<UpdateTimeSlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTimeSlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTimeSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
