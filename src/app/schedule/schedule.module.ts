import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ScheduleRoutingModule } from "./schedule-routing.module";
import { MyScheduleComponent } from "./my-schedule/my-schedule.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { UpdateTimeSlotsComponent } from "./update-time-slots/update-time-slots.component";
import { ScheduleComponent } from "./schedule.component";

@NgModule({
  declarations: [
    MyScheduleComponent,
    UpdateTimeSlotsComponent,
    ScheduleComponent,
  ],
  imports: [
    CommonModule,
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    ScheduleRoutingModule,
  ],
  exports: [MyScheduleComponent],
})
export class ScheduleModule {}
