import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ScheduleService {
  constructor(private http: HttpClient) {}

  InsertDoctorSchedule(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.insertDoctorSchedule,
      data
    );
  }

  scheduleAvailableDates(data) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.scheduleAvailableDates +
        "?Date=" +
        data
    );
  }

  getDoctorTimeSlotByDate(data) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.getDoctorTimeSlotByDate +
        "?selectedDate=" +
        data
    );
  }

  getDoctorDateWiseTimeShift(data) {
    return this.http.get<any>(
      environment.apiUrl +
        "/" +
        ApiEndPoint.doctorDateWiseTimeShift +
        "?Date=" +
        data
    );
  }

  removeDoctorSchedule(id: any) {
    return this.http.delete<any>(
      environment.apiUrl + "/" + ApiEndPoint.deleteDoctorSchedule + "?Id=" + id
    );
  }

  editSchedule(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.editDoctorSchedule,
      data
    );
  }
}
