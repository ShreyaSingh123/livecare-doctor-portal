import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../shared/guards/auth.guard";
import { MyScheduleComponent } from "./my-schedule/my-schedule.component";
import { ScheduleComponent } from "./schedule.component";
import { UpdateTimeSlotsComponent } from "./update-time-slots/update-time-slots.component";

const routes: Routes = [
  {
    path: "",
    component: MyScheduleComponent,
    canActivate: [AuthGuard],
    // children: [
    //   {
    //     path: "updatetimeslots",
    //     component: UpdateTimeSlotsComponent,
    //     canActivate: [AuthGuard],
    //   },
    // ],
  },
  {
    path: "updatetimeslots",
    component: UpdateTimeSlotsComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScheduleRoutingModule {}
