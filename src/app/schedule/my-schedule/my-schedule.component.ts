import { Component, OnInit } from "@angular/core";
import * as moment from "moment";
import { ScheduleService } from "../schedule.service";
import { DatePipe } from "@angular/common";
import Swal from "sweetalert2";
import { DatepickerDateCustomClasses } from "ngx-bootstrap/datepicker";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-my-schedule",
  templateUrl: "./my-schedule.component.html",
  styleUrls: ["./my-schedule.component.css"],
})
export class MyScheduleComponent implements OnInit {
  star: any;
  dateWiseSlots: Array<any>;
  newArray;
  calndar;
  firstTime: any;
  lastTime: any;
  startTime;
  endTime;
  nextMonth: Date;
  previousDate: Date;
  showTimeSlots: boolean = false;
  slotsByDate: any;
  showData: boolean = false;
  minDate = new Date();
  result: Array<any> = [];
  list: Array<any> = [];
  todayDate: any;
  starttest: any;
  timeSlotsData: any;
  endtest: any;
  test: any;
  test2: any;
  isShowSubmit: boolean = false;
  slots: any;
  value: any;
  selected: any;
  selectedObject: any;
  scheduleDate: any;
  dateCustomClasses: DatepickerDateCustomClasses[];
  timeSlotsByDate: any;
  updateedTimeSlots: any;
  selectedEnd: any;
  isShowCreate: any = false;
  showCalendar: boolean;

  finalList: Array<any> = [];
  arrayStart = [
    "00:00",
    "00:30",
    "01:00",
    "01:30",
    "02:00",
    "02:30",
    "03:00",
    "03:30",
    "04:00",
    "04:30",
    "05:00",
    "05:30",
    "06:00",
    "06:30",
    "07:00",
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:30",
    "11:00",
    "11:30",
    "12:00",
    "12:30",
    "13:00",
    "13:30",
    "14:00",
    "14:30",
    "15:00",
    "15:30",
    "16:00",
    "16:30",
    "17:00",
    "17:30",
    "18:00",
    "18:30",
    "19:00",
    "19:30",
    "20:00",
    "20:30",
    "21:00",
    "21:30",
    "22:00",
    "22:30",
    "23:00",
  ];

  arrayEnd = [
    "00:00",
    "00:30",
    "01:00",
    "01:30",
    "02:00",
    "02:30",
    "03:00",
    "03:30",
    "04:00",
    "04:30",
    "05:00",
    "05:30",
    "06:00",
    "06:30",
    "07:00",
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:30",
    "11:00",
    "11:30",
    "12:00",
    "12:30",
    "13:00",
    "13:30",
    "14:00",
    "14:30",
    "15:00",
    "15:30",
    "16:00",
    "16:30",
    "17:00",
    "17:30",
    "18:00",
    "18:30",
    "19:00",
    "19:30",
    "20:00",
    "20:30",
    "21:00",
    "21:30",
    "22:00",
    "22:30",
    "23:00",
    "23:30",
  ];

  constructor(
    private DatePipe: DatePipe,
    private scheduleService: ScheduleService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.previousDate = new Date();
    this.nextMonth = new Date();
    this.nextMonth.setMonth(this.nextMonth.getMonth() + 2);

    this.todayDate = moment().format("DD-MM-YYYY");
    this.scheduleService
      .scheduleAvailableDates(this.todayDate)
      .subscribe((res) => {
        this.value = res.data.map((x) => {
          return {
            date: new Date(
              moment
                .utc(x.fromTime, "DD-MM-YYYY HH:mm:ss")
                .local()
                .format("MM-DD-YYYY")
            ),
            classes: ["bg-warning"],
          };
        });

        this.dateCustomClasses = this.value.filter((s) => s.date >= new Date());
      });
    if (JSON.parse(localStorage.getItem("updateTimeSlots")) == null) {
      this.showData = false;
      this.showCalendar = true;
    } else {
      this.showTimeSlots = true;
      this.showCalendar = false;
      this.updateedTimeSlots = JSON.parse(
        localStorage.getItem("updateTimeSlots")
      );

      this.scheduleDate = moment(
        this.updateedTimeSlots["scheduleDate"],
        "MM-DD-YYYY"
      );
      this.selected = this.updateedTimeSlots["fromTime"];
      this.onChange(this.selected);
      this.isShowCreate = true;
      this.isShowSubmit = false;
      this.selectedEnd = this.updateedTimeSlots["toTime"];
      this.calndar = moment(
        this.updateedTimeSlots["scheduleDate"],
        "MM-DD-YYYY"
      ).format("DD-MM-YYYY");
      this.scheduleService
        .getDoctorTimeSlotByDate(this.calndar)
        .subscribe((res) => {
          this.slotsByDate = res.data.map((x) => {
            return {
              date: x.date,
              isSlotAvailable: x.isSlotAvailable,
              slotFrom: moment
                .utc(x.slotFrom, "DD-MM-YYYY HH:mm:ss")
                .local()
                .format("DD-MM-YYYY HH:mm:ss"),
              slotTo: moment
                .utc(x.slotTo, "DD-MM-YYYY HH:mm:ss")
                .local()
                .format("DD-MM-YYYY HH:mm:ss"),
              timeSlotId: x.timeSlotId,
            };
          });

          this.timeSlotsByDate = this.slotsByDate.filter((x) =>
            x.slotFrom.includes(this.calndar)
          );
          if (this.timeSlotsByDate.length !== 0) {
            this.showData = true;
          }

          localStorage.setItem("Date", this.calndar);
        });
    }
  }

  onChange(value) {
    // this.isShowSubmit = true;
    if (this.showCalendar) {
      this.isShowSubmit = true;
    }
    this.startTime = value + ":00";
    this.newArray = [];
    this.dateWiseSlots = [];
    this.result = [];
    this.endTime = "";
    this.newArray = this.arrayEnd.map((x) => x);
    var index = this.arrayStart.indexOf(value);
    this.dateWiseSlots = this.newArray.splice(index + 1);
    // this.selectedObject = this.dat[0];
  }
  onSelectEndDate(endData) {
    this.endTime = endData + ":00";
  }

  onSelectDate(data) {
    this.selected = "--";
    this.showTimeSlots = true;
    this.calndar = moment(data).format("DD-MM-YYYY");
    this.scheduleDate = moment(this.calndar, "DD-MM-YYYY").format("MM-DD-YYYY");

    this.scheduleService
      .getDoctorTimeSlotByDate(this.calndar)
      .subscribe((res) => {
        this.slotsByDate = res.data.map((x) => {
          return {
            date: x.date,
            isSlotAvailable: x.isSlotAvailable,
            slotFrom: moment
              .utc(x.slotFrom, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("DD-MM-YYYY HH:mm:ss"),
            slotTo: moment
              .utc(x.slotTo, "DD-MM-YYYY HH:mm:ss")
              .local()
              .format("DD-MM-YYYY HH:mm:ss"),
            timeSlotId: x.timeSlotId,
          };
        });

        this.timeSlotsByDate = this.slotsByDate.filter((x) =>
          x.slotFrom.includes(this.calndar)
        );
        if (this.timeSlotsByDate.length == 0) {
          this.showData = true;
        } else {
          this.router.navigateByUrl("my-schedule/updatetimeslots");
        }

        localStorage.setItem("Date", this.calndar);
      });
  }

  intervals(startString, endString) {
    var start = moment(startString, "DD-MM-YYYY HH:mm:ss");
    var end = moment(endString, "DD-MM-YYYY HH:mm:ss");
    this.starttest = this.DatePipe.transform(start, "dd-MM-yyyy");
    this.endtest = this.DatePipe.transform(end, "dd-MM-yyyy");
    start.minutes(Math.ceil(start.minutes() / 30) * 30);
    var current = moment(start);
    this.result = [];
    while (current <= end) {
      this.result.push(current.format("DD-MM-YYYY HH:mm:ss"));
      current.add(30, "minutes");
    }
    return this.result;
  }

  data() {
    if (this.endTime == "") {
      this.endTime = this.dateWiseSlots[0];
    }
    this.lastTime = moment(
      this.calndar + " " + this.endTime,
      "DD-MM-YYYY HH:mm:ss"
    )
      .utc()
      .format("DD-MM-YYYY HH:mm:ss");

    this.firstTime = moment(
      this.calndar + " " + this.startTime,
      "DD-MM-YYYY HH:mm:ss"
    )
      .utc()
      .format("DD-MM-YYYY HH:mm:ss");
    this.intervals(this.firstTime, this.lastTime);
    this.list = this.result.map((user, index) => {
      return {
        slotStart: user,
        slotEnd: this.result[index + 1] ? this.result[index + 1] : "",
      };
    });

    var lastIndex = this.list.length - 1;
    if (this.list[lastIndex].slotEnd == "") {
      this.list.splice(-1, 1);
    }

    function filterByValue(array, string) {
      return array.filter((o) =>
        Object.keys(o).some((k) =>
          o[k].toLowerCase().includes(string.toLowerCase())
        )
      );
    }
    var i;
    this.finalList = [];
    for (i = 0; i < this.list.length; i++) {
      if (
        this.list[i].slotStart.substring(0, 9) ==
        this.list[i].slotEnd.substring(0, 9)
      ) {
        this.finalList.push(this.list[i]);
      }
    }

    this.test = filterByValue(this.finalList, this.starttest);

    this.test2 = filterByValue(this.finalList, this.endtest);
    //this.test.splice(-1, 1);

    // this.test2.splice(0, 1);

    this.timeSlotsData = [];
  }

  onSubmit() {
    this.data();
    // if (this.endTime == "") {
    //   this.endTime = this.dat[0];
    // }
    // this.lastTime = moment(
    //   this.calndar + " " + this.endTime,
    //   "DD-MM-YYYY HH:mm:ss"
    // )
    //   .utc()
    //   .format("DD-MM-YYYY HH:mm:ss");

    // this.firstTime = moment(
    //   this.calndar + " " + this.startTime,
    //   "DD-MM-YYYY HH:mm:ss"
    // )
    //   .utc()
    //   .format("DD-MM-YYYY HH:mm:ss");
    // this.intervals(this.firstTime, this.lastTime);
    // this.list = this.result.map((user, index) => {
    //   return {
    //     slotStart: user,
    //     slotEnd: this.result[index + 1] ? this.result[index + 1] : "",
    //   };
    // });

    // var lastIndex = this.list.length - 1;
    // if (this.list[lastIndex].slotEnd == "") {
    //   this.list.splice(-1, 1);
    // }

    // function filterByValue(array, string) {
    //   return array.filter((o) =>
    //     Object.keys(o).some((k) =>
    //       o[k].toLowerCase().includes(string.toLowerCase())
    //     )
    //   );
    // }
    // var i;
    // this.finalList = [];
    // for (i = 0; i < this.list.length; i++) {
    //   if (
    //     this.list[i].slotStart.substring(0, 9) ==
    //     this.list[i].slotEnd.substring(0, 9)
    //   ) {
    //     this.finalList.push(this.list[i]);
    //   }
    // }

    // this.test = filterByValue(this.finalList, this.starttest);

    // this.test2 = filterByValue(this.finalList, this.endtest);
    // //this.test.splice(-1, 1);

    // // this.test2.splice(0, 1);

    // this.timeSlotsData = [];

    if (
      this.finalList[0].slotStart.substring(0, 9) ==
      this.finalList[this.finalList.length - 1].slotEnd.substring(0, 9)
    ) {
      this.timeSlotsData = {
        schedule: [
          {
            timeShift: {
              fromTime: this.finalList[0].slotStart,
              toTime: this.finalList[this.finalList.length - 1].slotEnd,
            },
            timeSlots: this.finalList,
          },
        ],
      };
    } else {
      this.timeSlotsData = {
        schedule: [
          {
            timeShift: {
              fromTime: this.test[0].slotStart,
              toTime: this.test[this.test.length - 1].slotEnd,
            },
            timeSlots: this.test,
          },
          {
            timeShift: {
              fromTime: this.test2[0].slotStart,
              toTime: this.test2[this.test2.length - 1].slotEnd,
            },
            timeSlots: this.test2,
          },
        ],
      };
    }
    this.spinner.show();
    this.scheduleService
      .InsertDoctorSchedule(this.timeSlotsData)
      .subscribe((res) => {
        if (res.status) {
          this.router.navigateByUrl("dashboard");
          this.spinner.hide();
          Swal.fire(res.message);
        } else {
          this.spinner.hide();
          Swal.fire(res.message);
        }
      });
  }

  onUpdate() {
    this.data();
    var id = this.updateedTimeSlots["id"];
    if (
      this.finalList[0].slotStart.substring(0, 9) ==
      this.finalList[this.finalList.length - 1].slotEnd.substring(0, 9)
    ) {
      this.timeSlotsData = {
        id: id,
        schedule: {
          timeShift: {
            fromTime: this.finalList[0].slotStart,
            toTime: this.finalList[this.finalList.length - 1].slotEnd,
          },
          timeSlots: this.finalList,
        },
      };
    } else {
      this.timeSlotsData = {
        id: id,
        schedule: [
          {
            timeShift: {
              fromTime: this.test[0].slotStart,
              toTime: this.test[this.test.length - 1].slotEnd,
            },
            timeSlots: this.test,
          },
          {
            timeShift: {
              fromTime: this.test2[0].slotStart,
              toTime: this.test2[this.test2.length - 1].slotEnd,
            },
            timeSlots: this.test2,
          },
        ],
      };
    }
    this.spinner.show();
    this.scheduleService.editSchedule(this.timeSlotsData).subscribe((res) => {
      if (res.status) {
        this.spinner.hide();
        Swal.fire(res.message);
      } else {
        this.spinner.hide();
        Swal.fire(res.message);
      }
    });
  }

  ngOnDestroy(): void {
    localStorage.removeItem("updateTimeSlots");
  }
}
