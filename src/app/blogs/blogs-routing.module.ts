import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//Components
import { BlogComponent, CreateBlogComponent } from ".";

//Shared
import { AuthGuard } from "../shared/guards/auth.guard";

const routes: Routes = [
  {
    path: "",
    component: BlogComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "create-blog",
    component: CreateBlogComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogsRoutingModule {}
