import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";

@Injectable({
  providedIn: "root",
})
export class BlogService {
  constructor(private http: HttpClient) {}

  //On create blog
  createBlog(data: any) {
    return this.http.post<any>(
      environment.apiUrl + "/" + ApiEndPoint.changePassword,
      data
    );
  }
}
