import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

//Modules
import { BlogsRoutingModule } from "./blogs-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

//Third
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { MaterialModule } from "../shared/main-material/material/material.module";
import SharedModule from "../shared/shared.module";

//Blogs Components
import { BlogComponent, CreateBlogComponent } from ".";

@NgModule({
  declarations: [BlogComponent, CreateBlogComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MaterialModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    PaginationModule.forRoot(),
    BlogsRoutingModule,
  ],
})
export class BlogsModule {}
