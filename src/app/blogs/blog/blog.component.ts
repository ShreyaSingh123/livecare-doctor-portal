import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/auth/auth.service";
import { environment } from "src/environments/environment";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.css"],
})
export class BlogComponent implements OnInit {
  List: Array<any>;
  rootUrl: any;
  loadFileUrl: any;
  totalCount: any;
  currentPage: number = 1;
  page: number = 1;
  pageSize: number = environment.defaultPageSize;

  constructor(private auth: AuthService, private spinner: NgxSpinnerService) {
    this.List = new Array<any>();
  }

  ngOnInit() {
    this.rootUrl = environment.baseImgUrl;
    this.getBlogsList();
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.getBlogsList();
  }

  getBlogsList() {
    this.spinner.show();
    this.rootUrl = environment.baseImgUrl;
    this.auth.getBlogList(this.page, this.pageSize).subscribe((res) => {
      this.List = res.data.dataList;
      this.totalCount = res.data.totalCount;
      this.spinner.hide();
    });
  }
}
