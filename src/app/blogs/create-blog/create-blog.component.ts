import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { BlogService } from "src/app/blogs/blog.service";
import { AuthService } from "src/app/auth/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-create-blog",
  templateUrl: "./create-blog.component.html",
  styleUrls: ["./create-blog.component.css"],
})
export class CreateBlogComponent implements OnInit {
  _fileData: File = null;
  uploadForm: FormGroup;
  constructor(
    private auth: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {}
  myForm: FormGroup = new FormGroup({
    Title: new FormControl(""),
    Description: new FormControl(""),
    BlogImage: new FormControl(""),
  });

  changeFile(fileInput: any) {
    this._fileData = fileInput.target.files[0] as File;
  }

  onSubmit() {
    this.spinner.show();
    var form = new FormData();
    form.append("Title", this.myForm.controls["Title"].value);
    form.append("Description", this.myForm.controls["Description"].value);
    form.append("BlogImage", this._fileData, this._fileData.name);
    this.auth.CreateBlog(form).subscribe((res) => {
      this.toastr.success(res.message);
      this.router.navigateByUrl("/blog");
    });
  }
}
