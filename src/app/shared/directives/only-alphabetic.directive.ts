import { Directive, HostListener, ElementRef } from "@angular/core";

@Directive({
  selector: "[appAlphabetOnly]",
})
export class AlphabetOnlyDirective {
  key;
  @HostListener("keydown", ["$event"]) onKeydown(event: KeyboardEvent) {
    this.key = event.keyCode;

    if (
      !(this.key == 8) &&
      !(this.key == 32) && // space
      !(this.key == 9) && // tab
      !(this.key > 64 && this.key < 91) && // upper alpha (A-Z)
      !(this.key > 96 && this.key < 123)
    ) {
      event.preventDefault();
    }
  }
}
