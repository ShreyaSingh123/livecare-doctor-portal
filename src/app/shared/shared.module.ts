import { NgModule } from "@angular/core";
import { OnlyNumberDirective } from "./directives/only-number.directive";
import { AlphabetOnlyDirective } from "./directives/only-alphabetic.directive";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [OnlyNumberDirective, AlphabetOnlyDirective],
  imports: [ReactiveFormsModule, FormsModule],
  exports: [OnlyNumberDirective, AlphabetOnlyDirective],
})
export default class SharedModule {}
