export interface FilterWithStatus {
  sortOrder: string;
  sortField: string;
  pageNumber: number;
  pageSize: number;
  searchQuery: string;
  filterBy: string;
  appointmentStatus: number;
}
