import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../auth/auth.service";
import { Router } from "@angular/router";
import * as AOS from "aos";
declare var $: any;

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  spinner: any;
  toaster: any;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    AOS.init();
    $(".flip").click(function (e) {
      $(".panel , .body-content").toggleClass("open");
    });
  }
  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl("/login");
  }
}
