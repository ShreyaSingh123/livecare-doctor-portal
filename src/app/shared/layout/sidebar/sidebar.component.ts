import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../auth/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { DashboardService } from "../../../dashboard/dashboard.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  doctorInfo: any;
  dashboardData: any;
  baseImgUrl = environment.baseImgUrl;
  specialityInfo: any;
  constructor(
    private authService: AuthService,
    private router: Router,
    private toaster: ToastrService,
    private spinner: NgxSpinnerService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit() {
    this.dashboardService.getDoctorDashboardInfo().subscribe((res: any) => {
      this.dashboardData = res.data;

      this.specialityInfo = this.dashboardData.doctorSpecialities;
    });
  }

  logout() {
    Swal.fire({
      title: "Are you sure you want to sign out?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      allowOutsideClick: false,
      backdrop: `
      rgba(0, 0, 0, 0.7)
      left top
      no-repeat
    `,
      cancelButtonText: "No",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.authService.logout().subscribe((res) => {
          if (res.status) {
            this.router.navigateByUrl("/login");
            this.spinner.hide();
            this.toaster.success(res.message);
          } else {
            this.spinner.hide();
            this.toaster.error(res.message);
          }
        });
      }
    });
  }
}
