export enum AppointmentStatus {
  Pending = 0, // 0. Pending appointments
  Confirmed, // 1. Confirmed or Accepted appointments
  Rejected, // 2. Deny or Rejected
  Cancelled, // 3. Confirmed appointment cancelled
  Reminder, //4. For Notification
  Completed, //5. Completed
}
