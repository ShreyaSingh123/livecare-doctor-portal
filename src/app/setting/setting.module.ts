import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SettingRoutingModule } from "./setting-routing.module";
import { SettingComponent } from "./setting.component";
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';

@NgModule({
  declarations: [SettingComponent, PrivacyPolicyComponent, TermsConditionComponent],
  imports: [CommonModule, SettingRoutingModule],
})
export class SettingModule {}
