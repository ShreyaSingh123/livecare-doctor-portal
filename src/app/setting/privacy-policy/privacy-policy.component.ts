import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { SettingService } from "../setting.service";

@Component({
  selector: "app-privacy-policy",
  templateUrl: "./privacy-policy.component.html",
  styleUrls: ["./privacy-policy.component.css"],
})
export class PrivacyPolicyComponent implements OnInit {
  Privacy: any;
  constructor(
    private settingService: SettingService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.settingService.getSetting().subscribe((res) => {
      this.Privacy = res.data.privacyPolicy;
      this.spinner.hide();
    });
  }
}
