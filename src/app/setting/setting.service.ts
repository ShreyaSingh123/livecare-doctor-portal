import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ApiEndPoint } from "../shared/enums/api-end-point.enum";

@Injectable({
  providedIn: "root",
})
export class SettingService {
  constructor(private http: HttpClient) {}

  //getSetting
  getSetting() {
    return this.http
      .get<any>(environment.apiUrl + "/" + ApiEndPoint.getSetting)
      .pipe(
        map((data: any) => {
          return data;
        })
      );
  }
}
