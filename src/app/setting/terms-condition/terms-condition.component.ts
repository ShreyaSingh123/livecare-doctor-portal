import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { SettingService } from "../setting.service";

@Component({
  selector: "app-terms-condition",
  templateUrl: "./terms-condition.component.html",
  styleUrls: ["./terms-condition.component.css"],
})
export class TermsConditionComponent implements OnInit {
  Terms: any;
  constructor(
    private settingService: SettingService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.settingService.getSetting().subscribe((res) => {
      this.Terms = res.data.termsConditions;
      this.spinner.hide();
    });
  }
}
