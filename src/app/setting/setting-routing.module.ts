import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../shared/guards/auth.guard";
import { PrivacyPolicyComponent } from "./privacy-policy/privacy-policy.component";
import { SettingComponent } from "./setting.component";
import { TermsConditionComponent } from "./terms-condition/terms-condition.component";

const routes: Routes = [
  {
    path: "",
    component: SettingComponent,
  },
  {
    path: "terms-condition",
    component: TermsConditionComponent,
  },
  {
    path: "privacy",
    component: PrivacyPolicyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingRoutingModule {}
